# Angular Teste para Desenvolvedor Frontend

Projeto de Angular com [Material UI](https://material.angular.io/) para Teste na Innovation Software

[Diego S.A.](https://www.linkedin.com/in/goethe0/)

## DEMO
[LIVE DEMO](http://goethedev.com/test-innovation/)

## Página URL
[criacaoapp.com](criacaoapp.com)

## Como rodar

Rodar `ng serve` ou `npm start`. Navegar para `http://localhost:4200/`.

